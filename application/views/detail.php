<div class="content-wrapper" style="background-color: white">
<div class="container">
   <?php foreach($prd as $td) : ?>
<!-- <div class="container mt-5"> -->
	<div class="row" style="margin-top: 2%;">
		<div class="col-md-4">
				<img src="<?= base_url() ?>/uploads/<?= $td->gambar ?>" class="rounded float-left" alt="..." style="width :100%">
		</div>
		<div class="col-md-5 animated fadeInUp slow">
			<h3><?= $td->nama_barang?></h3>
			<h5 class="text-secondary">Stok : <?= $td->stok?></h5>
			<br>
				<p class="text-danger">Informasi Prodak</p>
					<p><?= $td->deskripsi ?></p>
		</div>

		<div class="col-md-3">
			<div class="panel panel-danger" style="margin-top: 10%">
			  <div class="panel-heading">Total Harga</div>
			  <div class="panel-body">
			    <?='Rp. '.number_format($td->harga)?>
			  </div>
			</div>
			<a href="<?= base_url('shop/add_to_cart/'. $td->id) ?>">
			<button type="button" class="btn btn-primary btn-lg btn-block mb-2" style="margin-bottom: 2%">Beli Sekarang</button></a>
			<a href="<?= base_url() ?>" class="btn btn-danger btn-lg btn-block" role="button" aria-pressed="true">Kembali</a>
		</div>
	</div>
<!-- </div> -->
<?php endforeach ?>
</div>
</div>