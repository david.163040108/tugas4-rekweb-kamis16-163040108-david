
<!-- ================================ carosel ===================================================================================== -->

          <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                  <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                  <li data-target="#carousel-example-generic" data-slide-to="1" class=""></li>
                  <li data-target="#carousel-example-generic" data-slide-to="2" class=""></li>
                </ol>
                <div class="carousel-inner">
                  <div class="item active">
                    <img src="<?php echo base_url() ?>/assets/images/banner.jpg" alt="First slide">

                    <div class="carousel-caption">
                      First Slide
                    </div>
                  </div>
                  <div class="item">
                    <img src="<?php echo base_url() ?>/assets/images/banner2.jpg" alt="Second slide">

                    <div class="carousel-caption">
                      Second Slide
                    </div>
                  </div>
                  <div class="item">
                    <img src="<?php echo base_url() ?>/assets/images/banner3.jpg" alt="Third slide">

                    <div class="carousel-caption">
                      Third Slide
                    </div>
                  </div>
                </div>
                <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                  <span class="fa fa-angle-left"></span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                  <span class="fa fa-angle-right"></span>
                </a>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->

<div class="container" style="margin-top: 3%">

<div class="row iklan">
  <div class="col-md-3 ">
    <img src="<?php echo base_url(); ?>/assets/images/iklan1.jpg" alt="" style="width : 270px; height: 125px;">
  </div>  
  <div class="col-md-3 ">
    <img src="<?php echo base_url(); ?>/assets/images/iklan2.jpg" alt="" style="width : 270px; height: 125px;">
  </div>  
  <div class="col-md-3 ">
    <img src="<?php echo base_url(); ?>/assets/images/iklan3.jpg" alt="" style="width : 270px; height: 125px;">
  </div>  
  <div class="col-md-3 ">
    <img src="<?php echo base_url(); ?>/assets/images/iklan4.jpg" alt="" style="width : 270px; height: 125px;">
  </div>
</div>

</div>

<!-- ===================================================conten=============================================================================== -->
<div class="container">

<div class="row" style="margin-top: 3%; margin-right: -40px;">
  <?php foreach($prd as $td) : ?>
  <div class="col-sm-6 col-md-2" style="margin-right: 2.95%;">
    <div class="thumbnail" style=" width: 21rem; height: 25rem;">
      <img src="<?=base_url()?>/uploads/<?= $td->gambar;?>" alt="..." style=" height: 10rem">
      <div class="caption">
        <p><?= $td->nama_barang ?></p>
        <p><?='Rp. '.number_format($td->harga)?></p>
        <p><a href="<?= base_url('shop/add_to_cart/'. $td->id) ?>" class="btn btn-primary" role="button">Beli</a> <a href="<?= base_url('shop/detail/'. $td->id) ?>" class="btn btn-default" role="button">Detail</a></p>
      </div>
    </div>
  </div>
  <?php endforeach  ?>
</div>

</div>
