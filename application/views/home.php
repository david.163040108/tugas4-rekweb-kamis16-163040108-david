  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">


    <div class="row">
    
    	<?php foreach($produk as $td) : ?>
    	<div class="col-md-3">
			 <div class="card mt-4" style=" height: 20rem;">
        <img class="card-img-top" src="<?=base_url()?>/uploads/<?= $td->gambar;?>" alt="Card image cap" style=" height: 10rem">
			  <div class="card-body">
			    <h6 class="card-title"><?= $td->nama_barang ?></h6>
          <p class="card-text"><?= $td->harga ?></p>

          <a href="<?= base_url('shop/add_to_cart/'. $td->id) ?>" class="btn btn-primary">Beli</a>
			    <a href="<?= base_url('shop/detail/'. $td->id) ?>" class="btn btn-success">Detail</a>
			  </div>
			</div>
		</div>
		<?php endforeach  ?>
    </div>
      <!-- /.content -->
    </div>
    <!-- /.container -->
  </div>