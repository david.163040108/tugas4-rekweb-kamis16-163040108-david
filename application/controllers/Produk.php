<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produk extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		// ketikan source yang ada di modul
		$this->load->library('session');
		$this->load->library('cart');
		$this->API = "http://localhost/tugas3-rekweb-kamis16-163040108-david/";

	}

	public function index()
	{
		// ketikan source yang ada di modul
		$data['produk'] = json_decode($this->curl->simple_get($this->API. '/produk/' ));
		$data['prd'] = $data['produk'];
		$data['content'] = 'crud';
		$this->load->view('templates/template', $data);
	}



	public function search()
	{

	}

// tambah data
	public function create()
	{

		$this->_rules();

		$nama_barang = $this->input->post('nama_barang');
		$harga = $this->input->post('harga');
		$stok = $this->input->post('stok');
		//$gambar = $this->input->post('gambar');
		$deskripsi = $this->input->post('deskripsi');
		if($this->form_validation->run() == FALSE) {
		$this->add();

		}else{	
				//upload
				$config['upload_path']          = './uploads/';
                $config['allowed_types']        = 'gif|jpg|png';
                $config['max_size']             = '3000';
                $config['max_width']            = '3000';
                $config['max_height']           = '3000';

                $this->load->library('upload', $config);
                if ( ! $this->upload->do_upload())
                {
                	$this->add();
                }else{

                $file = $this->upload->data();
            	$gambar = $file['file_name'];
					$data = array(
						'nama_barang' => $nama_barang,
						'harga' => $harga,
						'stok' => $stok,
						'gambar' => $gambar,
						'deskripsi' => $deskripsi
					);	
					$status =$this->curl->simple_post($this->API . '/produk/', $data, array(CURLOPT_BUFFERSIZE => 10));
					

					if ($status) {
						$this->session->set_flashdata('status', '<div class="alert alert-success alert-dismissible">
		                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		                <h4><i class="icon fa fa-check"></i> Alert!</h4>
		                Produk Berhasil diTambah
		              </div>');
					}else{
						$this->session->set_flashdata('status', '<div class="alert alert-danger alert-dismissible">
		                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		                <h4><i class="icon fa fa-ban"></i> Alert!</h4>
		                Produk gagal di Tambahkan
		              </div>');
					}
				redirect('produk');
                }	
			
		}
	}

	public function edit($id)
	{

		$params = array('id' => $id);
		$data['prd'] = json_decode($this->curl->simple_get($this->API . '/produk/', $params));
		$data['content'] = 'ubah';
		$this->load->view('templates/template', $data);


	}

	public function update()
	{
			$this->_rules();
		$nama_barang = $this->input->post('nama_barang');
		$harga = $this->input->post('harga');
		$stok = $this->input->post('stok');
		$deskripsi = $this->input->post('deskripsi');
		$id = $this->input->post('id');


		if($this->form_validation->run() == FALSE) {

			$this->edit();
		}else{
			if($_FILES['userfile']['name'] != '')
			{
				$config['upload_path']          = './uploads/';
                $config['allowed_types']        = 'gif|jpg|png';
                $config['max_size']             = '3000';
                $config['max_width']            = '3000';
                $config['max_height']           = '3000';

                $this->load->library('upload', $config);
                if ( ! $this->upload->do_upload())
                {
                	$this->edit();
                }else{

                $file = $this->upload->data();
            	$gambar = $file['file_name'];
					$prd = array(
						'nama_barang' => $nama_barang,
						'harga' => $harga,
						'stok' => $stok,
						'gambar' => $gambar,
						'deskripsi' => $deskripsi,
						'id' => $id
					);	

					$status =$this->curl->simple_put($this->API . '/produk/', $prd, array(CURLOPT_BUFFERSIZE => 10));

					if ($status) {
						$this->session->set_flashdata('status', '<div class="alert alert-success alert-dismissible">
		                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		                <h4><i class="icon fa fa-check"></i> Alert!</h4>
		                Produk Berhasil diTambah
		              </div>');
					}else{
						$this->session->set_flashdata('status', '<div class="alert alert-danger alert-dismissible">
		                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		                <h4><i class="icon fa fa-ban"></i> Alert!</h4>
		                Produk gagal di Tambahkan
		              </div>');
					}
				redirect('produk');
                }	
			}else{
				$gambar = $this->input->post('gambar');
				$prd = array(
						'nama_barang' => $nama_barang,
						'harga' => $harga,
						'stok' => $stok,
						'gambar' => $gambar,
						'deskripsi' => $deskripsi,
						'id' => $id
					);	

						$status=$this->curl->simple_put($this->API . '/produk/', $prd, array(CURLOPT_BUFFERSIZE => 10));
				

					if ($status) {
						$this->session->set_flashdata('status', '<div class="alert alert-success alert-dismissible">
		                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		                <h4><i class="icon fa fa-check"></i> Alert!</h4>
		                Produk Berhasil diTambah
		              </div>');
					}else{
						$this->session->set_flashdata('status', '<div class="alert alert-danger alert-dismissible">
		                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		                <h4><i class="icon fa fa-ban"></i> Alert!</h4>
		                Produk gagal di Tambahkan
		              </div>');
					}
					redirect('produk');
			}
		}
	}

	public function delete($id)
	{
		$data['prd'] =json_decode($this->curl->simple_delete($this->API. '/produk/', array('id'=>$id), array(CURLOPT_BUFFERSIZE =>10)));
		$this->index();
	}


	public function _rules()
	{
		$this->form_validation->set_rules('nama_barang', 'nama produk', 'trim|required');
		$this->form_validation->set_rules('harga', 'harga', 'trim|required');
		$this->form_validation->set_rules('stok', 'stok', 'trim|required');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
	}

	//load View Tambah
	public function add(){
		$data['content'] = 'tambah';
		$this->load->view('templates/template', $data);
	}



}