<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shop extends CI_Controller {

        public function __construct()
    {
        parent::__construct();
        // ketikan source yang ada di modul
        $this->load->library('session');
        $this->load->library('cart');
        $this->API = "http://localhost/tugas3-rekweb-kamis16-163040108-david/";

    }

	public function index()
	{
        $data['produk'] = json_decode($this->curl->simple_get($this->API. '/produk/' ));
        $data['prd'] = $data['produk'];
        $data['content'] = 'beranda';
        $this->load->view('templates_home/template_home', $data);
    }




    public function detail($id)
    {
        $params = array('id' => $id);
        $data['prd'] = json_decode($this->curl->simple_get($this->API . '/produk/', $params));
        $data['content'] = 'detail';
         $this->load->view('templates_home/template_home', $data);

    }


    public function add_to_cart($id)
    {        
        $params = array('id' => $id);
        $da['prd'] = json_decode($this->curl->simple_get($this->API . '/produk/', $params));
    	$produk =$da['prd'];
    	$data = array(
        'id'      => $produk->id,
        'qty'     => 1,
        'price'   => $produk->harga,
        'name'    => $produk->nama_barang
		);

		$this->cart->insert($data);
		redirect('shop');
        echo json_encode(array("status"=>200, "mhs" =>$produk));
    }

    public function cart(){
    	
    	$data['content'] = 'show_cart';
        $this->load->view('templates_home/template_home', $data);
    }

    public function clear_cart()
    {
    	$this->cart->destroy();
    	redirect('shop/cart');
    }

    public function hapusCart($rowid){
		$this->cart->update(array('rowid' => $rowid, 'qty'=>0));
		$i = 0;
		foreach ($this->cart->contents() as $key) {
			$i++;
		}
		$data["total_cart"] = $i;
		$data['content'] = 'show_cart';
		$this->load->view('templates_home/template_home', $data);
	}
//=======================================================================================================
//login
    public function login()
    {
        $this->load->view('login');
    }
    
    public function ceklogin(){
        if(isset($_POST['login'])){
            $user = $this->input->post('user',true);
            $pass = $this->input->post('pass',true);
            $cek = $this->rafimodel->proseslogin($user, $pass);
            if($cek){
                $yglogin = $this->db->get_where('users',array('username'=>$user, 'password' => $pass))->row();
                $data = array(
                        'nama' => $yglogin->nama, 'email' => $yglogin->email, 'username' => $yglogin->username, 'level'=>$yglogin->level);
                $this->session->set_userdata($data);
                if($yglogin->level == 'admin'){
                  redirect(base_url());
                }elseif($yglogin->level == 'moderator'){
                 redirect(base_url('crud'));
                }
            }else{
                redirect(base_url('shop/login'));
            }
        }
    }

    function keluar(){
        $this->session->sess_destroy();
        $this->index();
    }

    public function registrasi(){
         $this->load->view('registrasi');
    }
    public function tambahUser(){

        $nama = $this->input->post('nama');
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $email = $this->input->post('email');
        $this->load->model('Crud_model');

        $data = array(
                        'nama' => $nama,
                        'username' => $username,
                        'password' => $password,
                        'email' => $email,
                        'level' => 'admin'
                    );  
        $this->Crud_model->insertUser($data);

    }


}